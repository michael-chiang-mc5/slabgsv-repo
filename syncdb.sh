#!/bin/bash
rm db.sqlite3
rm -rf mapPoints/migrations
rm -rf panoramas/migrations
rm -rf ocr/migrations
rm -rf signs/migrations
rm .lock

scp michaelc@138.197.220.71:/home/michaelc/dev/slabgsv-repo/db.sqlite3_thu db.sqlite3
scp -r michaelc@138.197.220.71:/home/michaelc/dev/slabgsv-repo/mapPoints/migrations mapPoints/migrations
scp -r michaelc@138.197.220.71:/home/michaelc/dev/slabgsv-repo/panoramas/migrations panoramas/migrations
scp -r michaelc@138.197.220.71:/home/michaelc/dev/slabgsv-repo/ocr/migrations ocr/migrations
scp -r michaelc@138.197.220.71:/home/michaelc/dev/slabgsv-repo/signs/migrations signs/migrations

