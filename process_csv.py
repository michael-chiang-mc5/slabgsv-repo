import csv 
from mysite.secret_keys import *
MIN_WORD_LENGTH = 3
MIN_NUM_CHAR = 2
MIN_CONFIDENCE = -1

ARABIC_UTF8_RANGE = [
    {'from': ord(u'\u0600'), 'to': ord(u'\u06ff')},
    {"from": ord(u"\u0750"), "to": ord(u"\u077f")},  
    {"from": ord(u"\u08a0"), "to": ord(u"\u08ff")},  
    {"from": ord(u"\ufb50"), "to": ord(u"\ufdff")},  
    {"from": ord(u"\ufe70"), "to": ord(u"\ufeff")},  
]

THAI_UTF8_RANGE = [
    {'from': ord(u'\u0e00'), 'to': ord(u'\u0e7F')},
]

ARMENIAN_UTF8_RANGE = [
    {'from': ord(u'\u0530'), 'to': ord(u'\u058f')},
]


JAPANESE_UTF8_RANGE = [
    {'from': ord(u'\u3040'), 'to': ord(u'\u309f')},         # Japanese Hiragana
    {"from": ord(u"\u30a0"), "to": ord(u"\u30ff")},         # Japanese Katakana
]

KOREAN_UTF8_RANGE = [
    {'from': ord(u'\u1100'), 'to': ord(u'\u11ff')},
    {"from": ord(u"\u3130"), "to": ord(u"\u318f")},
    {'from': ord(u'\ua960'), 'to': ord(u'\ua97f')},
    {'from': ord(u'\ua960'), 'to': ord(u'\ua97f')},
    {'from': ord(u'\uac00'), 'to': ord(u'\ud7af')},
    {'from': ord(u'\ud7b0'), 'to': ord(u'\ud7ff')},
]

# https://en.wikipedia.org/wiki/Unicode_block
CHINESE_UTF8_RANGE = [
  # {"from": ord(u"\u3300"), "to": ord(u"\u33ff")},         # compatibility ideographs
  # {"from": ord(u"\ufe30"), "to": ord(u"\ufe4f")},         # compatibility ideographs
  {"from": ord(u"\uf900"), "to": ord(u"\ufaff")},         # compatibility ideographs
  {"from": ord(u"\U0002F800"), "to": ord(u"\U0002fa1f")}, # compatibility ideographs
  {"from": ord(u"\u2e80"), "to": ord(u"\u2eff")},         # cjk radicals supplement
  {"from": ord(u"\u4e00"), "to": ord(u"\u9fff")}, # main block
  {"from": ord(u"\u3400"), "to": ord(u"\u4dbf")}, # rare block
  {"from": ord(u"\U00020000"), "to": ord(u"\U0002a6df")}, # rare
  {"from": ord(u"\U0002a700"), "to": ord(u"\U0002b73f")}, # rare
  {"from": ord(u"\U0002b740"), "to": ord(u"\U0002b81f")}, # rare
  {"from": ord(u"\U0002b820"), "to": ord(u"\U0002ceaf")}  # included as of Unicode 8.0
]

def check_character_language(txt, ranges):
    count = 0
    for char in txt:
        if any([range["from"] <= ord(char) <= range["to"] for range in ranges]):
            count += 1
    return count

def main():

    dictionary_english = load_dictionary('media/engmix.txt')
    dictionary_spanish = load_dictionary('media/espanol_utf8.txt')    
    with open('media/signs_final.csv', newline='') as f, open('media/signs_processed.csv','w') as output:
        reader = csv.reader(f, delimiter='\t')
        header = next(reader) # skip header row
        output_header = '\t'.join(header) + '\t' + \
                         'chinese\t' + \
                         'thai\t' + \
                         'armenian\t' + \
                         'arabic\t' + \
                         'japanese\t' + \
                         'korean\t' + \
                         'english\t' + \
                         'spanish\t' + \
                         'count_chinese_char\t' + \
                         'count_thai_char\t' + \
                         'count_armenian_char\t' + \
                         'count_japanese_char\t' + \
                         'count_korean_char\t' + \
                         'count_arabic_char\t' + \
                         'url_all_signs\t' + \
                         'url_one_sign\n'
        output.write(output_header)     
        
        for row1 in reader:

            # temporary variables
            language = {'chinese':0,
                        'thai':0,
                        'armenian':0,
                        'japanese':0,
                        'korean':0,
                        'spanish':0,
                        'english':0,
                        'arabic':0,}
            text = row1[0]
            ocr_pk = row1[3]
            x1 = row1[5]
            x2 = row1[6]
            y1 = row1[7]
            y2 = row1[8]

            # skip row if confidence below threshold
            if float(row1[10]) < MIN_CONFIDENCE:
                continue

            # count the number of unique chinese/thai/armenian/etc. characters.
            # assign language if >= MIN_NUM_CHAR
            unique_characters = "".join(set(text))
            num_unique_chinese_char = check_character_language(unique_characters, CHINESE_UTF8_RANGE)
            num_unique_thai_char = check_character_language(unique_characters, THAI_UTF8_RANGE)
            num_unique_armenian_char = check_character_language(unique_characters, ARMENIAN_UTF8_RANGE)
            num_unique_japanese_char = check_character_language(unique_characters, JAPANESE_UTF8_RANGE)
            num_unique_korean_char = check_character_language(unique_characters, KOREAN_UTF8_RANGE)
            num_unique_arabic_char = check_character_language(unique_characters, ARABIC_UTF8_RANGE)

            if num_unique_chinese_char >= MIN_NUM_CHAR:
                language['chinese'] = 1
            if num_unique_thai_char >= MIN_NUM_CHAR:
                language['thai'] = 1
            if num_unique_armenian_char >= MIN_NUM_CHAR:
                language['armenian'] = 1
            if num_unique_japanese_char >= MIN_NUM_CHAR:
                language['japanese'] = 1
            if num_unique_korean_char >= MIN_NUM_CHAR:
                language['korean'] = 1
            if num_unique_arabic_char >= MIN_NUM_CHAR:
                language['arabic'] = 1

            # check for english, spanish words
            words = text.split(' ')
            preprocessed_words = [preprocess(w)[1] for w in words if preprocess(w)[0]] # make sure word satisfies minimum length
            language['english'] = check_language(preprocessed_words,dictionary_english)
            language['spanish'] = check_language(preprocessed_words,dictionary_spanish)

            # skip entry if no language detected
            # if sum(language.values()) == 0:
            #     continue

            # write text file            
            # if language['armenian']:
            #     print(text, row1[3], row1[10])

            output_row = '\t'.join(row1) + \
                         str(language['chinese']) + '\t' + \
                         str(language['thai']) + '\t' + \
                         str(language['armenian']) + '\t' + \
                         str(language['arabic']) + '\t' + \
                         str(language['japanese']) + '\t' + \
                         str(language['korean']) + '\t' + \
                         str(language['english']) + '\t' + \
                         str(language['spanish']) + '\t' + \
                         str(num_unique_chinese_char) + '\t' + \
                         str(num_unique_thai_char) + '\t' + \
                         str(num_unique_armenian_char) + '\t' + \
                         str(num_unique_japanese_char) + '\t' + \
                         str(num_unique_korean_char) + '\t' + \
                         str(num_unique_arabic_char) + '\t' + \
                        'http://138.197.220.71:8889/ocr/detail/%s/\t' % ocr_pk + \
                        'http://138.197.220.71:8889/ocr/overlay/%s/%s/%s/%s/%s/\t' % (ocr_pk,x1,x2,y1,y2) + \
                         '\n'

            output.write(output_row)

            #print(header)
            #print(output_row)
            #import sys
            #sys.stdin.readline()
            # if language['arabic'] == 1:
            #     print(row1)
            #     return
            

def check_language(words, dictionary):
    for word in words:
        if word in dictionary:
            return 1
    return 0

def load_dictionary(filename):
    wordlist = list()
    with open(filename, encoding = "ISO-8859-1") as f:
        for line in f:
            wordlist.append(line.rstrip('\n'))
                    
    # convert list to dictoinary
    d = {word:word for word in wordlist}
    return d


def preprocess(word):
    """
    Returns True if word is a proper word 
      - Not a number
      - Minimum length
    Does not do dictionary check
    """

    import re
    word = re.sub(r"[^a-z]","",word.lower())
    if len(word) < MIN_WORD_LENGTH:
        return False,None
    else:
        return True,word

if __name__ == "__main__":
    main()