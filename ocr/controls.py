from panoramas.models import Panorama
import boto3
from mysite.settings import *
import ocr.nfov as nfov
from .models import *

def create_ocr_object(pano_image_name,depth_image_name,panoid,panorama_pk):
    """
    create Ocr object, which is a subimage of the panorama
    """
    # delete image files in ocr/tmp/
    filelist = [ f for f in os.listdir(BASE_DIR + 'ocr/tmp/') ]
    for f in filelist:
        if f == '.placeholder':
            continue
        print('deleting    ' , BASE_DIR + 'ocr/tmp/' + f)
        os.remove(os.path.join(BASE_DIR + 'ocr/tmp/', f))

    # download image to local
    pano_image_path = BASE_DIR + 'ocr/tmp/'+pano_image_name
    depth_image_path = BASE_DIR + 'ocr/tmp/'+depth_image_name
    s3 = boto3.client('s3', aws_access_key_id=AWS_KEY_ID,aws_secret_access_key=AWS_KEY_SECRET)
    s3.download_file(AWS_BUCKET_NAME, pano_image_name, pano_image_path)
    s3.download_file(AWS_BUCKET_NAME, depth_image_name, depth_image_path)

    """
    Crop panorama and depth images, and upload to AWS
    """
    # parameters
    direction = 0.5
    fov=[100,20]
    # this is for upload.
    s3 = boto3.client('s3',aws_access_key_id=AWS_KEY_ID, aws_secret_access_key=AWS_KEY_SECRET)
    outpath_pano = BASE_DIR + 'ocr/tmp/pano.jpg'
    outpath_depth= BASE_DIR + 'ocr/tmp/depth.tif'
    # create pano image subview and upload
    width_pano,height_pano = nfov.subimage_naive(imagepath = pano_image_path, outpath = outpath_pano, direction = direction,FOV=fov)
    pano_subimage_name = 'pano_'+panoid + '_direction=' + str(direction) + '.jpg' # this is the name of image on aws
    s3.upload_file(outpath_pano, AWS_BUCKET_NAME, pano_subimage_name)
    # create depth image subview and upload
    width_depth,height_depth = nfov.subimage_naive(imagepath = depth_image_path, outpath = outpath_depth, direction = direction, FOV=fov)
    depth_subimage_name = 'depth_'+panoid + '_direction=' + str(direction) + '.tif'
    s3.upload_file(outpath_depth, AWS_BUCKET_NAME, depth_subimage_name)

    # return dictionary data to create Ocr object
    return {'panorama_pk' : panorama_pk,
            'pano_image_name' : pano_subimage_name, 
            'depth_image_name' : depth_subimage_name,
            'width_depth' : width_depth,
            'width_pano' : width_pano,
            'height_depth' : height_depth,
            'height_pano' : height_pano,
            'direction' : direction,
            'fov_x' : fov[0],
            'fov_y' : fov[1],
            }


import json,urllib.request
import requests
# does not create signs
def worker_run_ocr():
    while(1):
        # get MapPoint data from master
        url = MYSITE_URL + 'panoramas/worker_get_panorama/' + str(WORKER_ID) + '/'
        print(url)        
        data = urllib.request.urlopen(url).read()
        data = data.decode("utf-8") 
        if data == "done":
            print("Done running ocr")
            break
        data = json.loads(data)
        print('Working on panorama pk=',data['panorama_pk'])
        # download and crop panorama
        ocr_data= create_ocr_object(data['pano_image_name'],
                                    data['depth_image_name'],
                                    data['panoid'],
                                    data['panorama_pk'])
        """
        run google ocr
        """
        print('running google ocr')
        from base64 import b64encode
        # convert image to bytes
        with open('ocr/tmp/pano.jpg', 'rb') as f:
            ctxt = b64encode(f.read()).decode()
            img_request = {'image': {'content': ctxt},
                                    'features': [{
                                    'type': 'DOCUMENT_TEXT_DETECTION', # DOCUMENT_TEXT_DETECTION, TEXT_DETECTION
                                    'maxResults': 1 }],
                                    'imageContext': { 'languageHints': OCR_LANGUAGE_CODES} # language codes: https://cloud.google.com/vision/docs/languages
                        }
        data = json.dumps({"requests": [img_request] }).encode()

        # send api call
        response = requests.post('https://vision.googleapis.com/v1/images:annotate',
                                data=data,
                                params={'key': GOOGLE_KEY_SECRET},
                                headers={'Content-Type': 'application/json'})

        if response.status_code != 200 or response.json().get('error'):
            print(response.text)
            google_ocr_json = ''
        else:
            response = response.json()['responses'][0]
            google_ocr_json = json.dumps(response)
        ocr_data['google_ocr_json'] = google_ocr_json

        print(ocr_data.keys())
        

        # Send data back to master u
        url = MYSITE_URL + 'ocr/create_Ocr/'
        headers = {'Content-Type': 'application/json', 'Accept':'application/json'}
        r = requests.post(url, data={'json_str':json.dumps(ocr_data)})
        print(r.status_code, r.reason)
        print(r.text[:300] + '...')



##
## Deprecated. Rectilinear correction does not seem to improve ocr results
##
# def create_ocr(panorama,direction,pano_image_path,depth_image_path):
#     s3 = boto3.client('s3',aws_access_key_id=AWS_KEY_ID, aws_secret_access_key=AWS_KEY_SECRET)

#     outpath_pano = BASE_DIR + 'ocr/tmp/pano.jpg'
#     outpath_depth= BASE_DIR + 'ocr/tmp/depth.tif'

#     # create pano image subview and upload
#     nfov.subimage(imagepath = pano_image_path, outpath = outpath_pano, direction = direction)
#     pano_subimage_name = 'pano_'+panorama.panoid + '_direction=' + str(direction) + '.jpg' # this is the name of image on aws
#     s3.upload_file(outpath_pano, AWS_BUCKET_NAME, pano_subimage_name)

#     # create depth image subview and upload
#     nfov.subimage(imagepath = depth_image_path, outpath = outpath_depth, direction = direction)
#     depth_subimage_name = 'depth_'+panorama.panoid + '_direction=' + str(direction) + '.tif'
#     s3.upload_file(outpath_depth, AWS_BUCKET_NAME, depth_subimage_name)

#     o = Ocr(panorama = panorama, pano_image_name = pano_subimage_name, depth_image_name=depth_subimage_name)
#     o.save()



def create_ocr_naive_deprecated(panorama,direction,pano_image_path,depth_image_path,fov):
    """
    Creates subview depth and pano images
    direction: 0.5 for straight ahead, 0.25 for left, 0.75 for right
    fov in percentage. fov = [100,50] means 360 degrees, 90 degrees
    """

    # this is for upload. Download handled one level up
    s3 = boto3.client('s3',aws_access_key_id=AWS_KEY_ID, aws_secret_access_key=AWS_KEY_SECRET)

    outpath_pano = BASE_DIR + 'ocr/tmp/pano.jpg'
    outpath_depth= BASE_DIR + 'ocr/tmp/depth.tif'

    # create pano image subview and upload
    width_pano,height_pano = nfov.subimage_naive(imagepath = pano_image_path, outpath = outpath_pano, direction = direction,FOV=fov)
    pano_subimage_name = 'pano_'+panorama.panoid + '_direction=' + str(direction) + '.jpg' # this is the name of image on aws
    s3.upload_file(outpath_pano, AWS_BUCKET_NAME, pano_subimage_name)

    # create depth image subview and upload
    width_depth,height_depth = nfov.subimage_naive(imagepath = depth_image_path, outpath = outpath_depth, direction = direction, FOV=fov)
    depth_subimage_name = 'depth_'+panorama.panoid + '_direction=' + str(direction) + '.tif'
    s3.upload_file(outpath_depth, AWS_BUCKET_NAME, depth_subimage_name)

    o = Ocr(panorama = panorama, 
            pano_image_name = pano_subimage_name, 
            depth_image_name=depth_subimage_name,
            width_depth=width_depth,
            width_pano=width_pano,
            height_depth=height_depth,
            height_pano=height_pano,
            direction=direction,
            fov_x=fov[0],
            fov_y=fov[1],
            )
    o.save()



def create_ocrs_deprecated(panorama):
    """
    create Ocr object, which is a subimage of the panorama
    """
    # delete image files in ocr/tmp/
    filelist = [ f for f in os.listdir(BASE_DIR + 'ocr/tmp/') ]
    for f in filelist:
        if f == '.placeholder':
            continue
        print('deleting    ' , BASE_DIR + 'ocr/tmp/' + f)
        os.remove(os.path.join(BASE_DIR + 'ocr/tmp/', f))

    # download image to local
    pano_image_path = BASE_DIR + 'ocr/tmp/'+panorama.pano_image_name
    depth_image_path = BASE_DIR + 'ocr/tmp/'+panorama.depth_image_name
    s3 = boto3.client('s3', aws_access_key_id=AWS_KEY_ID,aws_secret_access_key=AWS_KEY_SECRET)

    # TODO: hacky way to get thu's data working. Delete later
    # try:
    #     s3.download_file(AWS_BUCKET_NAME, panorama.pano_image_name, pano_image_path)
    #     s3.download_file(AWS_BUCKET_NAME, panorama.depth_image_name, depth_image_path)
    # except:
    #     s3.download_file('slabgsv-repo', panorama.pano_image_name, pano_image_path)
    #     s3.download_file('slabgsv-repo', panorama.depth_image_name, depth_image_path)


    # This replaces TODO
    s3.download_file(AWS_BUCKET_NAME, panorama.pano_image_name, pano_image_path)
    s3.download_file(AWS_BUCKET_NAME, panorama.depth_image_name, depth_image_path)

    # create ocr objects. Previously, we had two subviews, now we only have one
    #create_ocr_naive(p,direction = 0.25,pano_image_path=pano_image_path,depth_image_path=depth_image_path,fov=[25,20]) # left
    #create_ocr_naive(p,direction = 0.75, pano_image_path=pano_image_path,depth_image_path=depth_image_path, fov=[25,20]) # right
    create_ocr_naive_deprecated(panorama,direction = 0.5, pano_image_path=pano_image_path,depth_image_path=depth_image_path, fov=[100,20]) # no left/right, just one image

def run_google_ocr(ocr_pk,debug=False):
    """
    Runs google ocr on a given subimage specificed by ocr.pk
    """
    from base64 import b64encode
    import json
    import requests
    o = Ocr.objects.get(pk=ocr_pk)

    # download panorama subimage
    pano_subimage_path = 'ocr/tmp/'+o.pano_image_name
    s3 = boto3.client('s3', aws_access_key_id=AWS_KEY_ID,aws_secret_access_key=AWS_KEY_SECRET)
    s3.download_file(AWS_BUCKET_NAME, o.pano_image_name, pano_subimage_path)
    print("running google ocr on " + pano_subimage_path)

    # convert image to bytes
    with open(pano_subimage_path, 'rb') as f:
        ctxt = b64encode(f.read()).decode()
        img_request = {'image': {'content': ctxt},
                                'features': [{
                                'type': 'DOCUMENT_TEXT_DETECTION', # DOCUMENT_TEXT_DETECTION, TEXT_DETECTION
                                'maxResults': 1 }],
                                'imageContext': { 'languageHints': OCR_LANGUAGE_CODES} # language codes: https://cloud.google.com/vision/docs/languages
                      }
    data = json.dumps({"requests": [img_request] }).encode()

    # send api call
    response = requests.post('https://vision.googleapis.com/v1/images:annotate',
                            data=data,
                            params={'key': GOOGLE_KEY_SECRET},
                            headers={'Content-Type': 'application/json'})

    if response.status_code != 200 or response.json().get('error'):
        print(response.text)
        return
    else:
        response = response.json()['responses'][0]

    o.google_ocr_json = json.dumps(response)
    o.google_ocr_run = True
    o.save()



    