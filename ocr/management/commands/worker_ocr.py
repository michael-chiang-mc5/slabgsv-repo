from django.core.management.base import BaseCommand, CommandError
from ocr.models import Ocr
from panoramas.models import Panorama
from mapPoints.models import MapPoint
from ocr.controls import worker_run_ocr
from signs.controls import create_signs
from random import shuffle

class Command(BaseCommand):
    help = 'Create Ocr objects based on Panorama'

    def handle(self, *args, **options):
        worker_run_ocr()
