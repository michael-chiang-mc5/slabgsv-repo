from django.db import models
from panoramas.models import Panorama
from mysite.settings import *
import ast




class Ocr(models.Model):
    created_date = models.DateTimeField(auto_now_add=True)
    panorama = models.ForeignKey(Panorama, on_delete=models.CASCADE)
    pano_image_name = models.TextField(blank=True)
    depth_image_name = models.TextField(blank=True)
    google_ocr_json = models.TextField(blank=True)
    google_ocr_run = models.BooleanField(default=False)
    width_pano = models.IntegerField()  # this is the height of the subimage, NOT the entire depth image
    height_pano = models.IntegerField() # this is the height of the subimage, NOT the entire panorama image
    width_depth = models.IntegerField()
    height_depth = models.IntegerField()
    direction = models.FloatField() # This is the heading of the subimage, NOT the car. You have to add to get the true heading. Normalized [0,1]
                                    # 0.25 = left = -90, 0.75 = right = +90,
                                    # 0, -180    0.25,-90    0.5,0    0.75,90,   1,180
                                    #  y =  360*x - 180
    fov_x = models.FloatField() # this is the fov of the subimage, NOT the ocr sign
    fov_y = models.FloatField()
    lock_ocr = models.BooleanField(default=False)

    def __str__(self):
        return str(self.panorama)

    def url_pano_image(self):
        url = AWS_BASE_URL + self.pano_image_name
        return url        

    def url_depth_image(self):
        url = AWS_BASE_URL + self.depth_image_name
        return url        

    def run_google_ocr(self):
        self.google_ocr_run = True
        self.save()

    def json(self):
        """
        Returns json
        TODO: horribly unsafe
        """
        if self.google_ocr_json:
            return ast.literal_eval(self.google_ocr_json)
        else:
            return None

class QueueOcr(models.Model):
    ocr = models.ForeignKey(Ocr, on_delete=models.CASCADE)

    @staticmethod
    def ocr_list(n=10):
        try:            
            queue = QueueOcr.objects.all()[0:n]
        except:
            count = QueueOcr.objects.count()
            queue = QueueOcr.objects.all()[0:count]
        
        ocr_pks = queue.values_list('ocr', flat=True)
        ocr_queryset = Ocr.objects.filter(pk__in=ocr_pks)

        QueueOcr.objects.filter(pk__in=list( queue.values_list('pk', flat=True)  )).delete()
        return ocr_queryset

