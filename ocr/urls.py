from django.urls import path

from . import views

app_name = 'ocr'
urlpatterns = [
    path('overview/', views.overview, name='overview'),
    path('create_Ocr/', views.create_Ocr, name='create_Ocr'),
    path('detail/<int:ocr_pk>/', views.detail, name='detail'),
    path('worker_get_ocr/<int:worker_id>/', views.worker_get_ocr, name='worker_get_ocr'),
    path('overlay/<int:ocr_pk>/<int:x1>/<int:x2>/<int:y1>/<int:y2>/', views.overlay, name='overlay'),
]
