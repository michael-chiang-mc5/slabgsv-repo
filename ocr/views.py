from django.shortcuts import render
from django.http import HttpResponse
from panoramas.models import Panorama
import boto3
from mysite.settings import *
import ocr.nfov as nfov
from .models import *
from signs.models import Sign
import time
import random
import json
from django.core import serializers
import datetime

def worker_get_ocr(request, worker_id):
    # A clumsy way to solve concurrency conditions
    while(os.path.isfile('.lock')):
        print('database locked')
        time.sleep(random.uniform(0.1, 0.5))
    f = open(".lock","w")
    f.close()

    # # check if we are done with all ocr
    # # This is too slow to use. Maybe json text field too long?
    # count = len(Ocr.objects.filter(lock_ocr=False).values_list('pk',flat=True))
    # if count == 0:
    #    os.remove(".lock") 
    #    return HttpResponse("done")

    OVERRIDE = True
    SEND_LENGTH = 100
    if OVERRIDE is False:    
        try:
            pk_list = list(Ocr.objects.filter(lock_ocr=False)[0:SEND_LENGTH].values_list('pk',flat=True))
        except:
            # we are finished with queue!
            with open(BASE_DIR + "worker_log.txt", "a") as myfile:
                myfile.write('done with ocr to text\n')
            count = Ocr.objects.filter(lock_ocr=False).count()
            pk_list = list(Ocr.objects.filter(lock_ocr=False)[0:count].values_list('pk',flat=True))
        # update lock
        for pk in pk_list:
                o = Ocr.objects.get(pk=pk)
                o.lock_ocr = True
                o.save()
    else:
        ocr_list = QueueOcr.ocr_list(n=SEND_LENGTH)

    # serialize Ocr objects
    #o_list = Ocr.objects.filter(pk__in=pk_list).order_by('pk')
    o_list = ocr_list.order_by('pk')
    o_serialized = serializers.serialize("json", o_list)

    # serialize Panorama objects
    panorama_pk_list = o_list.values_list('panorama',flat=True)
    panorama_list = []
    for pk in panorama_pk_list:
        panorama_list.append(Panorama.objects.get(pk=pk))
    panorama_serialized = serializers.serialize("json", panorama_list)

    # debugging
    #print(pk_list)
    #print(panorama_pk_list)
    # log worker communication
    out_str = "sent ocr objects pk=[" + str(o_list.first().pk) + ' - ' + str(o_list.last().pk) + " to worker_id=" + str(worker_id) + " at " + str(datetime.datetime.now()) + "\n"
    with open(BASE_DIR + "worker_log.txt", "a") as myfile:
        myfile.write(out_str)

    # Remove lock against concurrency
    os.remove(".lock") 

    # combine Ocr, Panorama data
    data = {'ocr' : o_serialized,
            'panorama': panorama_serialized}
    return HttpResponse(json.dumps(data), content_type = "application/json")

# Create your views here.
def overview(request):
    count = len(Ocr.objects.all())
    return HttpResponse(count)

def overlay(request,ocr_pk,x1,x2,y1,y2):
    o = Ocr.objects.get(pk=ocr_pk)
    width = x2 - x1
    height = y2 - y1
    signs = [[ x1,y1,width,height,'' ]]
    context = {'ocr' : o,
               'signs' : signs,                                          
              }
    return render(request, 'ocr/detail.html',context)


def detail(request,ocr_pk):
    o = Ocr.objects.get(pk=ocr_pk)
    # parse json for signs
    from signs.controls import parse_json_to_sign
    texts,languages,boundingBoxes,confidences = parse_json_to_sign(o.json())
    signs = []
    for boundingBox,text in zip(boundingBoxes,texts):
        x1 = boundingBox[0]
        x2 = boundingBox[1]
        y1 = boundingBox[2]
        y2 = boundingBox[3]
        width = x2 - x1
        height = y2 - y1
        signs.append([x1,y1,width,height,text])
    context = {'ocr' : o,
               'signs' : signs,                                          
              }
    return render(request, 'ocr/detail.html',context)


# ocr/controls.py: worker_run_ocr uses this interface to communicate data back to master
# TODO: not secure
from django.views.decorators.csrf import csrf_exempt
@csrf_exempt
def create_Ocr(request):
    post = request.POST
    json_str = post.get('json_str')
    d = ast.literal_eval(json_str)
    print(d)

    # create Ocr object and save
    o = Ocr(panorama = Panorama.objects.get(pk=d['panorama_pk']), 
            pano_image_name = d['pano_image_name'], 
            depth_image_name= d['depth_image_name'],
            width_depth =d['width_depth'],
            width_pano =d['width_pano'],
            height_depth =d['height_depth'],
            height_pano =d['height_pano'],
            direction= d['direction'],
            fov_x= d['fov_x'],
            fov_y= d['fov_y'],
            google_ocr_json = d['google_ocr_json']
            )
    o.save()

    # return to worker
    return HttpResponse('Ocr object created')