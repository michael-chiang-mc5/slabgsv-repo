from ocr.models import Ocr
from .models import *
import boto3
import numpy as np

from mysite.settings import *

import json,urllib.request
import requests
from django.core import serializers
def worker_write_signs():
    while(1):
        # get MapPoint data from master
        url = MYSITE_URL + 'ocr/worker_get_ocr/' + str(WORKER_ID) + '/'
        data = urllib.request.urlopen(url).read()
        if data == "done":
            print("Done writing signs")
            break

        # get serialized ocr + panorama data
        data = json.loads(data)
        ocr_serialized = data['ocr']
        panorama_serialized = data['panorama']

        # parse ocr data
        for ocr_obj,panorama_obj in zip(serializers.deserialize("json", ocr_serialized), \
                                        serializers.deserialize("json", panorama_serialized)):
            ocr = ocr_obj.object
            panorama = panorama_obj.object
            
            print('working on ocr pk = ', ocr.pk)
            if len(ocr.google_ocr_json) == 0: # check if Ocr field is empty
                continue

            lines = create_signs_new(ocr,panorama) # each line corresponds to a single sign
        
            # write csv lines
            # stay within ram limits
            with open('media/signs_'+ str(WORKER_ID) + '.csv', 'a') as csvfile:
                for line in lines:
                    csvfile.write(line)
        



def create_signs_new(o,panorama):

    # delete image files in signs/tmp/
    filelist = [ f for f in os.listdir(BASE_DIR + 'signs/tmp/') ]
    for f in filelist:
        if f == '.placeholder':
            continue
        print('deleting    ' , BASE_DIR + 'signs/tmp/' + f)
        os.remove(os.path.join(BASE_DIR + 'signs/tmp/', f))

    # parse google ocr json
    json = o.json()
    if json==None:     # if no ocr result, skip
        return
    print('parsing ocr pk=',o.pk)
    texts,languages,boundingBoxes,confidences = parse_json_to_sign(json)

    signs_csv_lines = []
    for text,language,boundingBox,confidence in zip(texts,languages,boundingBoxes,confidences):
        # get sign bounding box
        x1 = boundingBox[0]
        x2 = boundingBox[1]
        y1 = boundingBox[2]
        y2 = boundingBox[3]
        
        # get distance of sign
        #s3 = boto3.client('s3', aws_access_key_id=AWS_KEY_ID,aws_secret_access_key=AWS_KEY_SECRET)
        #s3.download_file(AWS_BUCKET_NAME, o.depth_image_name, 'signs/tmp/' + o.depth_image_name)
        #import imageio as im
        #depth_img = im.imread('signs/tmp/' + o.depth_image_name)
        import imageio as im
        depth_img = im.imread('/media/michael/Elements/depth/' + o.depth_image_name)

        # rescale pano coordinates to depthmap coordinates
        scale_width = o.width_depth / o.width_pano
        scale_height = o.height_depth / o.height_pano
        x1_scale = round(x1 * scale_width)
        x2_scale = round(x2 * scale_width+1)
        y1_scale = round(y1 * scale_height)
        y2_scale = round(y2 * scale_height+1)
        x1_scale = bound(x1_scale, 0, o.width_depth - 1)
        x2_scale = bound(x2_scale, 0, o.width_depth - 1)
        y1_scale = bound(y1_scale, 0, o.height_depth - 1)
        y2_scale = bound(y2_scale, 0, o.height_depth - 1)
        img_crop = depth_img[y1_scale:y2_scale,x1_scale:x2_scale]
        img_crop = img_crop[~np.isnan(img_crop)]
        img_crop = np.ravel(img_crop)
        distance = np.median(img_crop)

        # calculate gps lon,lat based on distance
        #print(lat_car, lon_car, o.panorama.heading + 90, distance) 
        # create sign object
        s = Sign(ocr = o,
                 x1 = x1,
                 x2 = x2,
                 y1 = y1,
                 y2 = y2,
                 text = text,
                 languages = language,                 
                 distance = distance,
                 confidence= confidence,
                )
        from geographiclib.geodesic import Geodesic
        lon_car = panorama.lon
        lat_car = panorama.lat
        sign_heading = s.absolute_heading_new(panorama)

        # distance can be infinite, in which case we cannot calculate lat, lon
        try: 
            g = Geodesic.WGS84.Direct(lat_car, lon_car, sign_heading, distance,)
            s.lat_sign = g['lat2']
            s.lon_sign = g['lon2']
        except:
            print('infinite distance, lat/lon not saved')


        txt = s.csv_line(o,panorama)
        signs_csv_lines.append(txt)
    return signs_csv_lines






def parse_json_to_sign(json):

    rn_signtexts = []
    rn_signlanguages = []
    rn_boundingBox = []
    rn_confidence = []

    try:
        signs = json['fullTextAnnotation']['pages'][0]['blocks']
    except:
        return rn_signtexts,rn_signlanguages,rn_boundingBox,rn_confidence

    for sign in signs:
        #print(sign['boundingBox']['vertices'])

        # get bounding box
        x_vals = []
        y_vals = []
        for idx in range(0,4):
            try:
                x_vals.append( sign['boundingBox']['vertices'][idx]['x'] )
            except:
                print("Warning: empty x found, substituting zero")
                x_vals.append(0)
            try:
                y_vals.append( sign['boundingBox']['vertices'][idx]['y'] )
            except:
                print("Warning: empty x found, substituting zero")
                y_vals.append(0)
        x_1 = min(x_vals)
        x_2 = max(x_vals)
        y_1 = min(y_vals)
        y_2 = max(y_vals)

        aggregate_word = ''
        aggregate_languages = []
        for paragraph in sign['paragraphs']:
            for word in paragraph['words']:

                ## These are detected languages for the entire word.
                ## Deprecate in favor of detected charset of individual symbols
                # languages = [language['languageCode'] 
                #              for language in word['property']['detectedLanguages']]

                for symbol in word['symbols']:
                    character = symbol['text']
                    try:
                        languages = [language['languageCode'] 
                                    for language in symbol['property']['detectedLanguages']]                    
                    except:
                        languages = []
                    aggregate_word += character
                    aggregate_languages = list(set().union(aggregate_languages,languages))
                aggregate_word += ' '

        rn_signtexts.append(aggregate_word)
        rn_signlanguages.append(aggregate_languages)
        rn_boundingBox.append([x_1, x_2, y_1, y_2])
        rn_confidence.append(float(sign['confidence']))
    return rn_signtexts,rn_signlanguages,rn_boundingBox,rn_confidence
      

def create_signs(ocr_pk):
    """
    Parse json output for a given Ocr object, and create sign objects
    This includes getting distance from depth map
    """

    # delete image files in signs/tmp/
    filelist = [ f for f in os.listdir(BASE_DIR + 'signs/tmp/') ]
    for f in filelist:
        if f == '.placeholder':
            continue
        print('deleting    ' , BASE_DIR + 'signs/tmp/' + f)
        os.remove(os.path.join(BASE_DIR + 'signs/tmp/', f))

    #
    o = Ocr.objects.get(pk=ocr_pk)
    json = o.json()
    # if no ocr result, skip
    if json==None:
        return
    print('parsing ocr pk=',o.pk)
    texts,languages,boundingBoxes,confidences = parse_json_to_sign(json)


    for text,language,boundingBox,confidence in zip(texts,languages,boundingBoxes,confidences):
        # get sign bounding box
        x1 = boundingBox[0]
        x2 = boundingBox[1]
        y1 = boundingBox[2]
        y2 = boundingBox[3]
        
        # get distance of sign
        s3 = boto3.client('s3', aws_access_key_id=AWS_KEY_ID,aws_secret_access_key=AWS_KEY_SECRET)
        s3.download_file(AWS_BUCKET_NAME, o.depth_image_name, 'signs/tmp/' + o.depth_image_name)

        import imageio as im
        depth_img = im.imread('signs/tmp/' + o.depth_image_name)

        # rescale pano coordinates to depthmap coordinates
        scale_width = o.width_depth / o.width_pano
        scale_height = o.height_depth / o.height_pano

        x1_scale = round(x1 * scale_width)
        x2_scale = round(x2 * scale_width+1)
        y1_scale = round(y1 * scale_height)
        y2_scale = round(y2 * scale_height+1)
        x1_scale = bound(x1_scale, 0, o.width_depth - 1)
        x2_scale = bound(x2_scale, 0, o.width_depth - 1)
        y1_scale = bound(y1_scale, 0, o.height_depth - 1)
        y2_scale = bound(y2_scale, 0, o.height_depth - 1)

        img_crop = depth_img[y1_scale:y2_scale,x1_scale:x2_scale]
        img_crop = img_crop[~np.isnan(img_crop)]
        img_crop = np.ravel(img_crop)
        distance = np.median(img_crop)


        # calculate gps lon,lat based on distance
        #print(lat_car, lon_car, o.panorama.heading + 90, distance) 
        # create sign object
        s = Sign(ocr = o,
                 x1 = x1,
                 x2 = x2,
                 y1 = y1,
                 y2 = y2,
                 text = text,
                 languages = language,                 
                 distance = distance,
                 confidence= confidence,
                )
        from geographiclib.geodesic import Geodesic
        lon_car = o.panorama.lon
        lat_car = o.panorama.lat
        sign_heading = s.absolute_heading()

        # distance can be infinite, in which case we cannot calculate lat, lon
        try: 
            g = Geodesic.WGS84.Direct(lat_car, lon_car, sign_heading, distance,)
            s.lat_sign = g['lat2']
            s.lon_sign = g['lon2']
        except:
            print('infinite distance, lat/lon not saved')

        s.save()
        print('sign=',s.pk, ', ocr=',s.ocr.pk)

def bound(x, x_min, x_max):
    """
    Force x to fall within [x_min, x_max]
    """
    x = max(x, x_min)
    x = min(x, x_max)
    return x
