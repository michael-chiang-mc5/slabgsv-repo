from django.core.management.base import BaseCommand, CommandError
from signs.controls import create_signs
from ocr.models import Ocr
from signs.models import Sign
import csv

import pandas as pd
import numpy as np

class Command(BaseCommand):
    help = 'Run google ocr on Ocr objects'

    def handle(self, *args, **options):

        # clear previous AIN
        signs = Sign.objects.all()
        for index, sign in enumerate(signs):
            sign.ain = None
            sign.save()
            print(index)

        # read Thu's AIN file
        df = pd.read_csv('media/OCRSignsTable.csv')

        # set AIN
        for index, row in df.iterrows():
            sign_pk = row['sign_pk']
            ain = row['AIN']
            sign = Sign.objects.get(pk=sign_pk)
            sign.ain = ain
            sign.save()
            print(index)