from django.core.management.base import BaseCommand, CommandError
from signs.controls import create_signs
from ocr.models import Ocr
from signs.models import Sign
class Command(BaseCommand):
    help = 'Run google ocr on Ocr objects'

    def handle(self, *args, **options):
        Sign.objects.all().delete()
        for o in Ocr.objects.all():
            create_signs(ocr_pk=o.pk)
