from django.core.management.base import BaseCommand, CommandError
from signs.controls import worker_write_signs
from ocr.models import Ocr
from signs.models import Sign
import csv
from mysite.settings import *
from signs.controls import create_signs_new

class Command(BaseCommand):
    help = 'Write text file of signs'

    def handle(self, *args, **options):
        
        with open('media/final.csv', 'w') as csvfile:
            # write header
            csvfile.write('text\tlat_sign\tlon_sign\tocr_pk\theading\tx1\tx2\ty1\ty2\tdistance\tconfidence\tyear\tmonth\tlat_car\tlon_car\tnum_links\twidth_pano\theight_pano\tgoogle_languages\n')
            for ocr in Ocr.objects.all().iterator():
                panorama = ocr.panorama
                lines = create_signs_new(ocr,panorama) # each line corresponds to a single sign
                for line in lines:
                    csvfile.write(line)



