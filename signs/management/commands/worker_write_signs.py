from django.core.management.base import BaseCommand, CommandError
from signs.controls import worker_write_signs
from ocr.models import Ocr
from signs.models import Sign
import csv
from mysite.settings import *

class Command(BaseCommand):
    help = 'Write text file of signs'

    def handle(self, *args, **options):
        worker_write_signs()
