from django.shortcuts import render
from .models import *

def detail(request,sign_pk):
    s = Sign.objects.get(pk=sign_pk)
    context = {'sign' : s}
    return render(request, 'signs/detail.html',context)