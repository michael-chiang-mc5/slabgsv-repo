from django.urls import path
from django.conf.urls import url

from . import views

app_name = 'signs'
urlpatterns = [    
    path('detail/<int:sign_pk>/', views.detail, name='detail'),
    ]