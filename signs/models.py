from django.db import models
from mysite.settings import *
from ocr.models import Ocr

class Sign(models.Model):
    created_date = models.DateTimeField(auto_now_add=True)
    ocr = models.ForeignKey(Ocr, on_delete=models.CASCADE)
    x1 = models.IntegerField()
    x2 = models.IntegerField()
    y1 = models.IntegerField()
    y2 = models.IntegerField()
    text = models.TextField(blank=True)
    languages = models.TextField(blank=True)
    distance = models.FloatField(blank=True,null=True)
    lat_sign = models.FloatField(blank=True,null=True)
    lon_sign = models.FloatField(blank=True,null=True)
    confidence = models.FloatField(blank=True,null=True)
    ain = models.IntegerField(blank=True,null=True)
    
    def __str__(self):
        return str(self.y1)
    def width(self):
        return self.x2-self.x1
    def height(self):
        return self.y2-self.y1

    def x_fraction(self):
        return (float(self.x2) + float(self.x1))/2 / float(self.ocr.width_pano)

    def absolute_heading(self):
        """
        calculates the absolute heading of the sign (i.e., car + camera)
        """
        heading_car_degrees = self.ocr.panorama.heading
        heading_camera_degrees = 360 * self.ocr.direction - 180


        x_fraction_subimage = self.x_fraction()  # (float(self.x2) + float(self.x1))/2 / float(self.ocr.width_pano)
        # 0.5 -> 0
        # 0   -> -fov/2
        # 1   -> +fov/2
        # y =  fov*x - fov/2
        # fov is the % of dimx, i.e., 20 means 20% of dimx
        heading_sign_degrees = (self.ocr.fov_x*x_fraction_subimage  -  self.ocr.fov_x / 2)/100 * 360

        absolute_heading = heading_car_degrees + heading_camera_degrees + heading_sign_degrees
        return absolute_heading

    def absolute_heading_new(self,panorama):
        """
        calculates the absolute heading of the sign (i.e., car + camera)
        """
        heading_car_degrees = panorama.heading
        heading_camera_degrees = 360 * self.ocr.direction - 180


        x_fraction_subimage = self.x_fraction()  # (float(self.x2) + float(self.x1))/2 / float(self.ocr.width_pano)
        # 0.5 -> 0
        # 0   -> -fov/2
        # 1   -> +fov/2
        # y =  fov*x - fov/2
        # fov is the % of dimx, i.e., 20 means 20% of dimx
        heading_sign_degrees = (self.ocr.fov_x*x_fraction_subimage  -  self.ocr.fov_x / 2)/100 * 360

        absolute_heading = heading_car_degrees + heading_camera_degrees + heading_sign_degrees
        return absolute_heading

    def csv_line(self,ocr,panorama):

        #fieldnames = ('sign_pk','text','latitude','longitude','languages','image_url','image_pk','sign_heading','sign_overlay','x1','x2','y1','y2')
        #header = '%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n' % fieldnames

        # if self.lat_sign is not None:
        #     txt = '%d\t%s\t%f\t%f\t%s\t%s\t%d\t%f\t%s\t%d\t%d\t%d\t%d\n' % \
        #            (self.pk,self.text,self.lat_sign,self.lon_sign,self.languages,AWS_BASE_URL + self.ocr.pano_image_name, self.ocr.pk, self.x_fraction()*360,MYSITE_URL+'viewer/overlaySigns/'+str(self.ocr.pk)+'/',self.x1,self.x2,self.y1,self.y2  ) 
        # else:
        #     txt = '%d\t%s\t%s\t%s\t%s\t%s\t%d\t%f\t%s\t%d\t%d\t%d\t%d\n' % \
        #            (self.pk,self.text,self.lat_sign,self.lon_sign,self.languages,AWS_BASE_URL + self.ocr.pano_image_name, self.ocr.pk, self.x_fraction()*360,MYSITE_URL+'viewer/overlaySigns/'+str(self.ocr.pk)+'/',self.x1,self.x2,self.y1,self.y2)

        txt = self.text +'\t'+\
              str(self.lat_sign) +'\t'+\
              str(self.lon_sign) +'\t'+\
              str(self.ocr.pk) +'\t'+\
              str(self.x_fraction()*360) +'\t'+\
              str(self.x1) +'\t'+\
              str(self.x2) +'\t'+\
              str(self.y1) +'\t'+\
              str(self.y2) +'\t'+\
              str(self.distance) +'\t'+\
              str(self.confidence) +'\t'+\
              str(panorama.year) +'\t'+\
              str(panorama.month) +'\t'+\
              str(panorama.lat) +'\t'+\
              str(panorama.lon) +'\t'+\
              str(panorama.num_links) +'\t'+\
              str(ocr.width_pano) +'\t'+\
              str(ocr.height_pano) +'\t'+\
              str(self.languages) + '\t'+\
              '\n'

        # remove problematic characters
        import re
        txt = re.sub('[\'\"]', '', txt)
        # return
        return txt

