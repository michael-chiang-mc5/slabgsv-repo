from django.db import models


class MapPoint(models.Model):
    created_date = models.DateTimeField(auto_now_add=True)
    object_id = models.IntegerField() # julia's index in shapefile
    lon = models.FloatField()
    lat = models.FloatField()
    lock = models.BooleanField(default=False)
    shapefile_name = models.TextField(blank=True)
    year1 = models.IntegerField(default=2017)
    year2 = models.IntegerField(default=2018)
    def __str__(self):
        return 'pk= '+ str(self.pk) +'   ,   (lon,lat) = ' + str(self.lon) + ", "+str(self.lat)
        
