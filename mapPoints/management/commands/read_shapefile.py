from django.core.management.base import BaseCommand, CommandError
import geopandas as gpd
from mysite.settings import *
from mapPoints.models import MapPoint
#from mapPoints.models import Question as Poll

class Command(BaseCommand):
    help = 'Reads shapefile of GPS coordinates and creates mapPoint objects\n. \
            Example usage: python manage.py read_shapefile \
                            --filename PanoramaPoints_100ft.shp \
                            --year1 2010 --year2 2011'

    def add_arguments(self, parser):
        #parser.add_argument('filename', nargs=1, type=str)
        parser.add_argument('--filename', nargs='+', type=str)
        parser.add_argument('--year1', nargs='+', type=int)        
        parser.add_argument('--year2', nargs='+', type=int)        

    def handle(self, *args, **options):
        
        print(options)
        filename = options['filename'][0]
        year1 = options['year1'][0]
        year2 = options['year2'][0]
        filepath = MEDIA_ROOT + filename
        df = gpd.read_file(filepath) 

        for i, row in df.iterrows():
            lat = row['geometry'].y
            lon = row['geometry'].x
            m = MapPoint(lat=lat,lon=lon,shapefile_name=filename,object_id=i,year1=year1,year2=year2)
            m.save()
            print(i)





# 1) 1039 E Valley Blvd, San Gabriel, California (OBJECTIDs: 285770, 285771, 285772)
# 2) 505 S Vermont Ave, Los Angeles, California (OBJECTIDs: 187691, 187692, 187693)
# 3) 869 S Western Ave, Los Angeles, CA 90005 (OBJECTIDs: 257504, 257505, 257506, 257507)
# 4) 448 S Broadway, Los Angeles, California (OBJECTIDs: 183667, 183668, 183669)
