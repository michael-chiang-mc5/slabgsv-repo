from django.urls import path

from . import views

app_name = 'mapPoints'
urlpatterns = [
    path('overview/', views.overview, name='overview'),
    path('worker_get_mapPoint/<int:worker_id>/', views.worker_get_mapPoint, name='worker_get_mapPoint'),
]
