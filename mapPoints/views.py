from django.shortcuts import render
from django.http import HttpResponse
from .models import *
import json
import os.path
import time
import random
from random import randint
import datetime
from mysite.settings import *

def overview(request):
    num_mapPoints = MapPoint.objects.count()
    return HttpResponse(num_mapPoints)

# Worker uses this view to get a MapPoint data for panorama scraping
def worker_get_mapPoint(request, worker_id):

    # A clumsy way to solve concurrency conditions
    while(os.path.isfile('.lock')):
        print('database locked')
        time.sleep(random.uniform(0, 1))
    f = open(".lock","w")
    f.close()

    # check if we are done with all MapPoint
    count = MapPoint.objects.filter(lock=False).count()
    if count == 0:
        os.remove(".lock") 
        return HttpResponse("done")

    # Get a MapPoint to pass to worker
    # TODO filter out MapPoint with panorama objects
    # USE DJANGO-MODELS-HOW-TO-FILTER-NUMBER-OF-FOREIGNKEY-OBJECTS
    # AND BY YEAR

    # # prioritize Thu's points
    # for object_id in [330733,240887,328305,48474,345895,222274,188657,292108,94433,139702,214347,73959,303447,248421,182321,244844,83403,300288,205196,45606,22703,153447,6882,24207,122233,322271,249853,6609,90079,318039,289826,339586,162438,247262,26364,134283,46286,297120,296622,347832,214795,237267,22042,72124,106155,340726,257834,225520,148794,3735,212677,335917,116625,274628,14657,339990,211815,75933,257927,28646,239632,145899,344438,140540,161953,127475,45498,4726,347445,211370,268426,203420,134234,298595,137158,126188,78112,88203,154483,349035,207623,288002,156898,167089,135781,255137,164904,85508,131901,173519,131074,85465,206129,274052,128132,62449,295911,185071,279496,322268,42956,67401,334520,93785,194919,253840,16809,200135,101922,303824,274232,299597,161620,201649,206469,76156,10663,256569,214471,324276,241485,239351,240178,201633,166317,243595,101940,197791,192269,101123,218669,38148,277212,91862,1705,240211,294124,75635,48427,271116,335729,221258,244656,105641,312786,64400,94113,150298,291199,226469,308468,268160,205998,263044,192274,185089,129871,118677,14865,63716,140881,290816,89447,320769,315415,320558,68087,191004,217705,7240,320880,112486,304605,268286,200016,66676,320088,45984,214419,70043,241091,314347,196991,135267,179500,334117,175508,4051,179953,45624,287838,169408,162100,165220,7754,259796,105752,232142,315445,326725,107002,43716,96434,91907,117066,233641,264496,197300,260773,60959,222959,73693,9205,313588,170381,115524,7340,73217,192008,278749,85976,199390,327975,106212,150166,6761,176274,196867,163600,168900,323901,147804,238563,113541,249268,338793,189710,136688,28778,77127,128434,107736,263476,288682,66272,277634,283263,202642,119913,261769,261630,253640,105620,68333,76311,17426,2415,229987,216687,71329,186054,56124,24464,318023,135127,285278,260450,107211,45077,193938,320147,313048,333759,323182,111086,193821,91689,22704,112798,275722,168858,196013,12844,24206,173763,338448,130218,270370,325016,190042,75261,340876,233165,250620,297207,107152,139295,22926,208981,11781]:
    #     try:
    #         m = MapPoint.objects.filter(lock=False).get(object_id=object_id)
    #         break
    #     except:
    #         continue
    # try:
    #     print('Found Thu point ',m.object_id)
    # except:
    #     m = MapPoint.objects.filter(lock=False).first()
    m = MapPoint.objects.filter(lock=False).first()
    m.lock =  True
    m.save()
    # log worker communication
    out_str = "sent mapPoint pk=" + str(m.pk) + " to worker_id=" + str(worker_id) + " at " + str(datetime.datetime.now()) + "\n"
    with open(BASE_DIR + "worker_log.txt", "a") as myfile:
        myfile.write(out_str)

    # Remove lock against concurrency
    os.remove(".lock") 

    # Send MapPoint data to worker
    data = {'lon' : m.lon,
            'lat' : m.lat,
            'pk'  : m.pk,
            'year1': m.year1,
            'year2': m.year2,}
    return HttpResponse(json.dumps(data), content_type = "application/json")
