rm db.sqlite3
rm -rf mapPoints/migrations
rm -rf panoramas/migrations
rm -rf ocr/migrations
rm -rf signs/migrations
python manage.py makemigrations mapPoints
python manage.py makemigrations panoramas
python manage.py makemigrations ocr
python manage.py makemigrations signs
python manage.py migrate
rm .lock


python manage.py read_shapefile Export_PP1207_50.shp # downloads all points
python manage.py worker_create_panoramas
python manage.py create_ocrs # This creates subimages. It does NOT run google ocr. Deletes previous ocr
python manage.py run_google_ocr # This runs google ocr on each subimage. It rewrites any previously run google ocr
python manage.py create_signs    # Deletes previous signs

# http://127.0.0.1:8000/ocr/subimage/




# http://127.0.0.1:8000/viewer/mapPoint/1/
