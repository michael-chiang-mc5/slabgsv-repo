#!/bin/bash

python manage.py collectstatic
pkill -f /home/michaelc/anaconda3/envs/slabgsv-repo/bin/gunicorn 
# python manage.py migrate --run-syncdb
gunicorn mysite.wsgi --bind 127.0.0.1:8001 --daemon --log-file ~/dev/logs/slabgsv-repo.log --workers=1
