from django.core.management.base import BaseCommand, CommandError
from panoramas.controls import worker_create_panoramas
class Command(BaseCommand):
    help = 'worker gets MapPoint from master, creates panorama, uploads to master'

    def handle(self, *args, **options):
        worker_create_panoramas()