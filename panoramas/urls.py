from django.urls import path
from django.conf.urls import url

from . import views

app_name = 'panoramas'
urlpatterns = [
    path('create_Panorama/', views.create_Panorama, name='create_Panorama'),

    path('overview/', views.overview, name='overview'),
    path('detailNeighborhood/<int:neighborhood_pk>/', views.detailNeighborhood, name='detailNeighborhood'),
    path('googleViewer/<int:panorama_pk>/', views.googleViewer, name='googleViewer'),
    path('detail/<int:panorama_pk>/', views.detail, name='detail'),
    path('worker_get_panorama/<int:worker_id>/', views.worker_get_panorama, name='worker_get_panorama'),
    ]