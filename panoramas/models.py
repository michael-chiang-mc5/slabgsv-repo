from django.db import models
from mapPoints.models import MapPoint
from mysite.settings import *
from django.forms.models import model_to_dict

class Panorama(models.Model):
    # used for parallel processing
    lock = models.BooleanField(default=False)
    #signs_written = models.BooleanField(null=True) # this is used to determine whether signs have already been written to text file. The reason why we don't do this in the Ocr object is because filter is very slow

    # associated mapPoint
    mapPoint = models.ForeignKey(MapPoint, on_delete=models.CASCADE)

    # date that this object was created
    created_date = models.DateTimeField(auto_now_add=True)

    # date that car acquired panorama
    year = models.IntegerField()
    month = models.IntegerField()

    # metadata for panorama image
    pano_image_width = models.FloatField()
    pano_image_height = models.FloatField()
    pano_image_name = models.TextField(blank=True)

    # metadata for depth image
    depth_image_width = models.FloatField()
    depth_image_height = models.FloatField()
    depth_image_name = models.TextField(blank=True)

    # miscellaneous
    heading = models.FloatField()
    lon = models.FloatField()
    lat = models.FloatField()
    panoid = models.TextField(blank=True)
    address = models.TextField(blank=True)
    num_links = models.IntegerField()


    def __str__(self):
        return self.pano_image_name

    def url_pano_image(self):
        url = AWS_BASE_URL + self.pano_image_name
        return url

    def url_depth_image(self):
        url = AWS_BASE_URL + self.depth_image_name
        return url

    def info_all(self):
        p = Panorama.objects.filter(pk=self.pk)[0]
        return model_to_dict(p)


class NeighboringPanoramas(models.Model):
    mapPoint = models.ForeignKey(MapPoint, on_delete=models.CASCADE)
    panoramas = models.ManyToManyField(Panorama,related_name='panoramas')
    centerPanorama = models.ForeignKey(Panorama, on_delete=models.CASCADE)