from .streetscrape import *
from geographiclib.geodesic import Geodesic
from mysite.settings import *
from .models import *

def nearestPanoid(lat,lon,year_min,year_max):
    # Get a list of panoramas that are close to GPS lon/lat
    panos = panoids(lat, lon)

    # Filter list of panoramas by year
    panos_filtered = [pano for pano in panos if 'year' in pano.keys()]
    panos_filtered = [pano for pano in panos_filtered if pano['year'] >= year_min and pano['year'] <= year_max ]
    if len(panos_filtered) == 0:
        print('Warning: no filtered panorama found')
        return None

    # search for closest panorama
    distance_meters = np.array([ Geodesic.WGS84.Inverse(pano['lat'],pano['lon'],lat,lon)['s12'] for pano in panos_filtered ])
    pano_nearest = panos_filtered[np.argmin(distance_meters)]
    panoid = pano_nearest['panoid']
    return panoid



def scrapeStreetview_helper(depth_map,metadata):
    if metadata is None:
        return None
    """
    Returns None if there was a problem
    """
    # compute depth map
    depthdata = decode_depth_map(depth_map)
    depth_image_name = 'depth_'+metadata['panoid'] + \
                       '_lon='+str(metadata['lon']) + \
                       '_lat='+str(metadata['lat']) + \
                       '_year='+str(metadata['year']) + \
                       '.tif'
    construct_depth_img(depthdata,
                        BASE_DIR+'panoramas/tmp/'+depth_image_name)

    # tiles is a list of (tile_x,tile_y,panoid,url)
    tiles = tiles_info(metadata['panoid'],len_x = metadata['pano_len_x'], len_y = metadata['pano_len_y'], zoom=GSV_SCRAPE_ZOOM) 

    # Construct panorama in 'output/'
    pano_image_name = 'pano_'+metadata['panoid']+'_lon='+str(metadata['lon'])+'_lat='+str(metadata['lat'])+'_year='+str(metadata['year'])+'.jpg'
    download_tiles(tiles,BASE_DIR+'panoramas/tmp/',disp=1,truncated=False)
    try:
        stitch_tiles(metadata['panoid'],tiles,BASE_DIR+'panoramas/tmp/',BASE_DIR+'panoramas/tmp/' + pano_image_name,
                    resize=True,truncated=False,
                    tile_sz = metadata['pano_tile_sz'], len_x = metadata['pano_len_x'], len_y = metadata['pano_len_y'])
    except:
        print("Error stitching tiles")
        return None
    # clean up metadata
    metadata['depth_width'] = depthdata[0][0]
    metadata['depth_height'] = depthdata[0][1]
    metadata['pano_image_name'] = pano_image_name
    metadata['depth_image_name'] = depth_image_name
    return metadata

def scrapeStreetview(panoid, neighbors=True):

    # delete temporary files
    delete_files(BASE_DIR+'panoramas/tmp/')

 
    # Get panorama metadata (heading, address, etc)
    metadata,depth_map = getMetadata(panoid,GSV_SCRAPE_ZOOM) # dict.keys() = [len_x , len_y, tile_sz, date, heading, address, links_panoids]
    if metadata is None:
        return [None] 

    # get data for neighboring panoramas
    if neighbors:
        # center point
        metadata_center = scrapeStreetview_helper(depth_map,metadata) # metadata_center = None if there was a problem with download or stitching
        metadata_list = [metadata_center]
        for panoid_neighbor in metadata['links_panoids']:
            # neighbors
            metadata_neighbor,depth_map_neighbor = getMetadata(panoid_neighbor,GSV_SCRAPE_ZOOM) # dict.keys() = [len_x , len_y, tile_sz, date, heading, address, links_panoids]
            metadata_neighbor = scrapeStreetview_helper(depth_map_neighbor,metadata_neighbor)
            metadata_list.append(metadata_neighbor)
        return metadata_list
    # no neighbors
    else:
        metadata = scrapeStreetview_helper(depth_map,metadata)
        return [metadata]


import json,urllib.request
import requests
import boto3
def worker_create_panoramas(neighbors = True):
    while(1):
        # get MapPoint data from master
        url = MYSITE_URL + 'mapPoints/worker_get_mapPoint/' + str(WORKER_ID) + '/'
        print(url)
        data = urllib.request.urlopen(url).read()
        data = data.decode("utf-8") 
        if data == "done":
            print("Done scraping GSV for all MapPoints")
            break
        data = json.loads(data)
        lon = data['lon']
        lat = data['lat']
        year1 = int(data['year1'])
        year2 = int(data['year2'])        
        print('Working on MapPoint pk=',data['pk'], ', year=[',year1,'-',year2,']')

        # nearest panoid
        panoid = nearestPanoid(lat,lon,year1,year2)
        if panoid is None:
            print('No nearest panoid found for panorama pk = ', data['pk'], ' and (lat,lon)=',lat,lon,'year=[', year1, ',', year2, ']')
            continue
        print('nearest panoid = ',panoid)
        #return


        # create Panorama object with associated images
        # metadata = None if no panorama found
        metadata_list = scrapeStreetview(panoid, neighbors = neighbors)
        if None in metadata_list: # check if there was a problem with stitching or download in any of the neighboring panoramas
            print('Problem with download or stitching for panorama pk = ', data['pk'], ' and (lat,lon)=', lat, lon,'year=[', year1, ',', year2, ']')
            continue
        else:
            for i,metadata in enumerate(metadata_list):
                # upload images to amazon s3
                s3 = boto3.client('s3',aws_access_key_id=AWS_KEY_ID, aws_secret_access_key=AWS_KEY_SECRET)
                s3.upload_file(BASE_DIR+'panoramas/tmp/'+metadata['pano_image_name'], AWS_BUCKET_NAME, metadata['pano_image_name'] )
                s3.upload_file(BASE_DIR+'panoramas/tmp/'+metadata['depth_image_name'], AWS_BUCKET_NAME, metadata['depth_image_name'] )
                metadata['mapPoint_pk'] = data['pk']
                print('uploaded ' , metadata['pano_image_name'])
                print('uploaded ' , metadata['depth_image_name'])

            # Send data back to master u
            url = MYSITE_URL + 'panoramas/create_Panorama/'
            headers = {'Content-Type': 'application/json', 'Accept':'application/json'}

            #r = requests.post(url,data=metadata)
            r = requests.post(url, data={'json_str':json.dumps(metadata_list)})
            print(r.status_code, r.reason)
            print(r.text[:300] + '...')
