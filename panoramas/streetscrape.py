# -*- coding: utf-8 -*-
"""
:Module: ``streetview``
:Author: `Adrian Letchford <http://www.dradrian.com>`_
:Organisation: `Warwick Business School <http://www.wbs.ac.uk/>`_, `University of Warwick <http://www.warwick.ac.uk/>`_.
This is a light module for downloading photos from Google street view. The
functions allow you to retrieve current and **old** photos.
The photos on Google street view are panoramas and are refered to as such.
However, you have the option of downloading flat photos, or panoramas.
Retrieving photos is a two step process. First, you must translate GPS
coordinates into panorama ids. The following code retrieves a list of
the closest panoramas giving you their id and date:
>>> import streetview
>>> panoids = streetview.panoids(lat, lon)
You can then use the panorama ids to download photos with the following
function:
>>> streetview.api_download(panoid, heading, flat_dir, key)
"""

# https://medium.com/@nocomputer/creating-point-clouds-with-google-street-view-185faad9d4ee
# https://github.com/gronat/streetget/tree/master/streetget

import re
from datetime import datetime
import time
import shutil
import itertools
from PIL import Image
from io import BytesIO
import os
from urllib.request import urlopen
import json
from struct import Struct
import base64
import zlib
import numpy as np
import requests

def decode_depth_map(depth_data):
    """
    docstring
    """
    # See https://github.com/gronat/streetget/tree/master/streetget
    encoded = depth_data
    # Decode
    encoded += '=' * (len(encoded) % 4)
    encoded = encoded.replace('-', '+').replace('_', '/')
    data = base64.b64decode(encoded.encode('ascii'))
    data = zlib.decompress(data)
    # data = encoded.decode('base64').decode('zip')       # deprecated in python 3
    #print(type(data))
    # Read header
    #hsize = ord(str(data[0]))                # header size in bytes
    #hsize = 8
    hsize = data[0]
    fmt = Struct('< x 3H B') # little endian, padding byte, 3x unsigned short int, unsigned char
    n_planes, width, height, offset = fmt.unpack(data[:hsize])
    #print(n_planes, width, height, offset)

    # Read plane labels
    n = width * height
    fmt = Struct('%dB' % n)
    lbls = fmt.unpack(data[offset:offset+fmt.size])
    offset += fmt.size

    # Read planes
    fmt = Struct('< 4f')                # little endian, 4 signed floats
    planes = []
    for i in range(n_planes):
        unpacked = fmt.unpack(data[offset:offset+fmt.size])
        planes.append((unpacked[:3], unpacked[3]))
        offset += fmt.size

    depthdata = (width, height), lbls, planes
    return depthdata

def construct_depth_img(depthdata,filepath):
    size, lbls, planes = depthdata
    w, h = size
    pi = np.pi

    #print(w,h)

    # Rays from camera center in spherical coordinates
    y, x = np.indices((h, w))           # grid of coordinates
    offset = pi/2                       # no idea why not pi,
    yaw = (w-1 - x) * 2*pi / (w-1) + offset
    pitch = (h-1 - y) * pi / (h-1)      # 0 down, pi/2 horizontal, pi up

    # Rays from spherical to cartesian
    v = np.array([
        np.sin(pitch) * np.cos(yaw),
        np.sin(pitch) * np.sin(yaw),
        np.cos(pitch)
    ])
    v = v.transpose(1, 2, 0)

    # w x h x 3 normal, resp. w x h x 1 distance
    n = np.array([planes[i][0] for i in lbls]).reshape((h, w, 3))
    d = np.array([planes[i][1] for i in lbls]).reshape((h, w))
    d[d == 0] = np.nan

    # distance from camera centetr, ray inersection with plane
    depthimg = d / np.abs(np.sum(v * n, axis=2))
    img = Image.fromarray(depthimg)
    img.save(filepath)



# get the heading for a panorama
from math import pow
def getMetadata(panoid,zoom):
    """
    docstring
    """
    url = 'https://maps.google.com/cbk?'\
          'output=json&cb_client=apiv3&v=4&dm=1&pm=1&ph=1&hl=en&panoid=%s' % panoid
    print('google metadata url: ', url)
    try:
        response = urlopen(url)
    except:
        print('metadata not found')
        return None,None
    string = response.read().decode('utf-8')
    j = json.loads(string)
    # some useful metadata
    image_width = int(j['Data']['image_width'])
    image_height = int(j['Data']['image_height'])
    tile_width = int(j['Data']['tile_width'])
    tile_height = int(j['Data']['tile_height'])
    image_date = j['Data']['image_date']
    heading = j['Projection']['pano_yaw_deg']
    try:
        address = j['Location']['description']
    except:
        address = ''
    try:
        address += ", " + j['Location']['region']
    except:
        address += ''
    try:
        links_panoids = [ link['panoId'] for link in j['Links'] ]
    except: links_panoids = []
    if zoom == 0:
        pano_len_x = 1
        pano_len_y = 1
    elif zoom == 1:
        pano_len_x = 2
        pano_len_y = 1
    elif zoom == 2:
        pano_len_x = 4
        pano_len_y = 2
    elif zoom == 3:
        pano_len_x = 7
        pano_len_y = 4
    elif zoom == 4:
        pano_len_x = 13
        pano_len_y = 7
    elif zoom == 5:
        #pano_len_x = 26
        #pano_len_y = 13
        pano_len_x = image_width / tile_width
        pano_len_y = image_height / tile_height
    return {'panoid':panoid,
            'image_date':image_date,
            'month':int(image_date.split('-')[1]),
            'year':int(image_date.split('-')[0]),
            'lon':j['Location']['original_lng'],
            'lat':j['Location']['original_lat'], # TODO: I'm not sure whether to use original_lat or lat
            'pano_len_x' : int(pano_len_x), 
            'pano_len_y' : int(pano_len_y),
            'pano_image_width' : int(image_width),
            'pano_image_height' : int(image_height),
            'pano_tile_sz' : int(tile_width),
            'heading' : heading,
            'address' : address,
            'num_links' : len(links_panoids),
            'links_panoids' : links_panoids,} , j['model']['depth_map']

def _panoids_url(lat, lon):
    """
    Builds the URL of the script on Google's servers that returns the closest
    panoramas (ids) to a give GPS coordinate.
    """
    url = "https://maps.googleapis.com/maps/api/js/GeoPhotoService.SingleImageSearch?pb=!1m5!1sapiv3!5sUS!11m2!1m1!1b0!2m4!1m2!3d{0:}!4d{1:}!2d50!3m10!2m2!1sen!2sGB!9m1!1e2!11m4!1m3!1e2!2b1!3e2!4m10!1e1!1e2!1e3!1e4!1e8!1e6!5m1!1e2!6m1!1e2&callback=_xdc_._v2mub5"
    return url.format(lat, lon)


def _panoids_data(lat, lon, proxies=None):
    """
    Gets the response of the script on Google's servers that returns the
    closest panoramas (ids) to a give GPS coordinate.
    """
    url = _panoids_url(lat, lon)
    return requests.get(url, proxies=None)


def panoids(lat, lon, closest=False, disp=False, proxies=None):
    """
    Gets the closest panoramas (ids) to the GPS coordinates.
    If the 'closest' boolean parameter is set to true, only the closest panorama
    will be gotten (at all the available dates)
    """

    resp = _panoids_data(lat, lon)

    # Get all the panorama ids and coordinates
    # I think the latest panorama should be the first one. And the previous
    # successive ones ought to be in reverse order from bottom to top. The final
    # images don't seem to correspond to a particular year. So if there is one
    # image per year I expect them to be orded like:
    # 2015
    # XXXX
    # XXXX
    # 2012
    # 2013
    # 2014
    pans = re.findall('\[[0-9]+,"(.+?)"\].+?\[\[null,null,(-?[0-9]+.[0-9]+),(-?[0-9]+.[0-9]+)', resp.text)
    pans = [{
        "panoid": p[0],
        "lat": float(p[1]),
        "lon": float(p[2])} for p in pans]  # Convert to floats

    # Remove duplicate panoramas
    pans = [p for i, p in enumerate(pans) if p not in pans[:i]]

    if disp:
        for pan in pans:
            print(pan)

    # Get all the dates
    # The dates seem to be at the end of the file. They have a strange format but
    # are in the same order as the panoids except that the latest date is last
    # instead of first.
    dates = re.findall('([0-9]?[0-9]?[0-9])?,?\[(20[0-9][0-9]),([0-9]+)\]', resp.text)
    dates = [list(d)[1:] for d in dates]  # Convert to lists and drop the index
    
    if len(dates) > 0:
        # Convert all values to integers
        dates = [[int(v) for v in d] for d in dates]

        # Make sure the month value is between 1-12
        dates = [d for d in dates if d[1] <= 12 and d[1] >= 1]

        # The last date belongs to the first panorama
        year, month = dates.pop(-1)
        pans[0].update({'year': year, "month": month})

        # The dates then apply in reverse order to the bottom panoramas
        dates.reverse()
        for i, (year, month) in enumerate(dates):
            pans[-1-i].update({'year': year, "month": month})

    # # Make the first value of the dates the index
    # if len(dates) > 0 and dates[-1][0] == '':
    #     dates[-1][0] = '0'
    # dates = [[int(v) for v in d] for d in dates]  # Convert all values to integers
    #
    # # Merge the dates into the panorama dictionaries
    # for i, year, month in dates:
    #     pans[i].update({'year': year, "month": month})

    # Sort the pans array
    def func(x):
        if 'year'in x:
            return datetime(year=x['year'], month=x['month'], day=1)
        else:
            return datetime(year=3000, month=1, day=1)
    pans.sort(key=func)

    if closest:
        return [pans[i] for i in range(len(dates))]
    else:
        return pans


def tiles_info(panoid,len_x=32,len_y=16,zoom=5):
    """
    Generate a list of a panorama's tiles and their position.
    The format is (x, y, filename, fileurl)
    """
    # old url: "http://cbk0.google.com/cbk?output=tile&panoid={0:}&zoom=5&x={1:}&y={2:}"

    image_url = "https://geo1.ggpht.com/cbk?cb_client=maps_sv.tactile&authuser=0&hl=en&gl=us&panoid={0:}&output=tile&x={1:}&y={2:}&zoom={3:}&nbt&fover=2"
    # Example usage of url:
    # https://geo1.ggpht.com/cbk?cb_client=maps_sv.tactile&authuser=0&hl=en&gl=us&panoid=bP-KRUyaZjwttyV55xsntw&output=tile&x=25&y=8&zoom=5&nbt&fover=2

    # The tiles positions
    coord = list(itertools.product(range(len_x), range(len_y)))
    print(len_x,len_y)
    tiles = [(x, y, "%s_%dx%d.jpg" % (panoid, x, y), image_url.format(panoid, x, y, zoom)) for x, y in coord]
    return tiles

def download_tiles(tiles, directory, disp=False,truncated=False):
    """
    Downloads all the tiles in a Google Stree View panorama into a directory.
    Params:
        tiles - the list of tiles. This is generated by tiles_info(panoid).
        directory - the directory to dump the tiles to.
    """

    if truncated:
        tiles = [tile for tile in tiles if tile[1]>=5 and tile[1]<=10]


    for i, (x, y, fname, url) in enumerate(tiles):

        if disp and i % 20 == 0:
            print("Image %d (%d)" % (i, len(tiles)))

        # Try to download the image file
        count = 0
        while True:
            try:
                response = requests.get(url, stream=True)
                break
            except requests.ConnectionError:
                count = count + 1
                print("Connection error. Trying again in 2 seconds.")
                time.sleep(5)
                if count > 3:
                    print("Careful not to get banned")
                    exit(1)

        with open(directory + '/' + fname, 'wb') as out_file:
            shutil.copyfileobj(response.raw, out_file)
        del response


    
def stitch_tiles(panoid, tiles, directory, output_image_path,resize=True,truncated=False,tile_sz=512,len_x=32,len_y=16):
    """
    Stiches all the tiles of a panorama together. The tiles are located in
    `directory'.
    """
    # TODO: truncated is not robust to different len_y
    if truncated:
        tiles = [tile for tile in tiles if tile[1]>=5 and tile[1]<=10]
        panorama = Image.new('RGB', (len_x*tile_sz, 6*tile_sz))
    else:
        panorama = Image.new('RGB', (len_x*tile_sz, len_y*tile_sz))


    for x, y, fname, url in tiles:

        fname = directory + fname
        tile = Image.open(fname)

        actual_width,actual_height=tile.size
        if actual_width != tile_sz and resize:
            tile = tile.resize((tile_sz,tile_sz),Image.ANTIALIAS)

        if truncated:
            panorama.paste(im=tile, box=(x*tile_sz, (y-5)*tile_sz))
        else:
            panorama.paste(im=tile, box=(x*tile_sz, y*tile_sz))

        del tile


    panorama.save(output_image_path)
    del panorama


def delete_tiles(tiles, directory):
    for x, y, fname, url in tiles:
        os.remove(directory + "/" + fname)


def api_download(panoid, heading, flat_dir, key, width=640, height=640,
                 fov=120, pitch=0, extension='jpg', year=2017, fname=None):
    """
    Download an image using the official API. These are not panoramas.
    Params:
        :panoid: the panorama id
        :heading: the heading of the photo. Each photo is taken with a 360
            camera. You need to specify a direction in degrees as the photo
            will only cover a partial region of the panorama. The recommended
            headings to use are 0, 90, 180, or 270.
        :flat_dir: the direction to save the image to.
        :key: your API key.
        :width: downloaded image width (max 640 for non-premium downloads).
        :height: downloaded image height (max 640 for non-premium downloads).
        :fov: image field-of-view.
        :image_format: desired image format.
        :fname: file name
    You can find instructions to obtain an API key here: https://developers.google.com/maps/documentation/streetview/
    """
    if not fname:
        fname = "%s_%s_%s" % (year, panoid, str(heading))
    image_format = extension if extension != 'jpg' else 'jpeg'

    url = "https://maps.googleapis.com/maps/api/streetview"
    params = {
        # maximum permitted size for free calls
        "size": "%dx%d" % (width, height),
        "fov": fov,
        "pitch": pitch,
        "heading": heading,
        "pano": panoid,
        "key": key
    }

    response = requests.get(url, params=params, stream=True)
    try:
        img = Image.open(BytesIO(response.content))
        filename = '%s/%s.%s' % (flat_dir, fname, extension)
        img.save(filename, image_format)
    except:
        print("Image not found")
        filename = None
    del response
    return filename


def download_flats(panoid, flat_dir, key, width=400, height=300,
                   fov=120, pitch=0, extension='jpg', year=2017):
    for heading in [0, 90, 180, 270]:
        api_download(panoid, heading, flat_dir, key, width, height, fov, pitch, extension, year)



def delete_files(directory):
    filelist = [ f for f in os.listdir(directory) ]
    for f in filelist:
        if f == '.placeholder':
            continue
        os.remove(os.path.join(directory, f))