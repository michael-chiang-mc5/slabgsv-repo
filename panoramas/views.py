from django.shortcuts import render
from django.http import HttpResponse
from django.db import transaction
import time
import os.path
import random
import json
from django.views.decorators.csrf import csrf_exempt
from .models import *
from mapPoints.models import MapPoint
import ast
import datetime

# Worker uses this view to get Panorama data for ocr processing
def worker_get_panorama(request, worker_id):

    # A clumsy way to solve concurrency conditions
    while(os.path.isfile('.lock')):
        print('database locked')
        time.sleep(random.uniform(0.1, 0.5))
    f = open(".lock","w")
    f.close()

    # check if we are done with all panoramas
    count = Panorama.objects.filter(lock=False,year__gte=0).count()
    if count == 0:
        os.remove(".lock") 
        return HttpResponse("done")

    # Get a Panorama to pass to worker
    #p = Panorama.objects.filter(lock=False).first()
    # Filter by year, 

    while(1):
        p = Panorama.objects.filter(lock=False,year__gte=0).first()
        if p.ocr_set.all().count() > 0: # ocr was previously run
            print("ocr previously run on panorama.pk=",p.pk)
            p.lock =  True
            p.save()
        else: # ocr was not run previously
            break            

    # lock panorama
    p.lock =  True
    p.save()
    # log worker communication
    out_str = "sent panorama pk=" + str(p.pk) + " to worker_id=" + str(worker_id) + " at " + str(datetime.datetime.now()) + "\n"
    with open(BASE_DIR + "worker_log.txt", "a") as myfile:
        myfile.write(out_str)

    # Remove lock against concurrency
    os.remove(".lock") 

    # Send MapPoint data to worker
    data = {'panorama_pk' : p.pk,
            'pano_image_name' : p.pano_image_name,
            'depth_image_name'  : p.depth_image_name,
            'panoid': p.panoid,}

    # _ = Panorama.objects.filter(lock=False,year__gte=2010)
    # print(_.count())
    # print(p.pk)
    #             
    return HttpResponse(json.dumps(data), content_type = "application/json")




# panorama/controls.py: worker_create_panoramas uses this interface to communicate data back to master
# TODO: not secure
@csrf_exempt
def create_Panorama(request):


    post = request.POST
    json_str = post.get('json_str')
    d = ast.literal_eval(json_str)

    for i,pano_metadata in enumerate(d):
        p = Panorama(mapPoint = MapPoint.objects.get(pk=pano_metadata['mapPoint_pk']),
                    year = pano_metadata['year'],
                    month = pano_metadata['month'],
                    pano_image_width = pano_metadata['pano_image_width'],
                    pano_image_height = pano_metadata['pano_image_height'],
                    depth_image_width = pano_metadata['depth_width'],
                    depth_image_height = pano_metadata['depth_height'],
                    heading = pano_metadata['heading'],
                    lon = pano_metadata['lon'],
                    lat = pano_metadata['lat'],
                    panoid = pano_metadata['panoid'],
                    address = pano_metadata['address'],
                    num_links = pano_metadata['num_links'],
                    pano_image_name = pano_metadata['pano_image_name'],
                    depth_image_name = pano_metadata['depth_image_name'],)
        p.save()
        print(p)

        if i == 0:
            neighboringPanoramas = NeighboringPanoramas(mapPoint = MapPoint.objects.get(pk=pano_metadata['mapPoint_pk']),
                                                        centerPanorama = p,)
            neighboringPanoramas.save()
        neighboringPanoramas.panoramas.add(p)
    neighboringPanoramas.save()
    return HttpResponse('Panorama objects created')

def overview(request):
    num_panoramas = Panorama.objects.count()
    return HttpResponse(num_panoramas)

def detail(request,panorama_pk):
    p = Panorama.objects.get(pk=panorama_pk)
    context = {'panorama' : p}
    return render(request, 'panoramas/detail.html',context)
def googleViewer(request,panorama_pk):
    p = Panorama.objects.get(pk=panorama_pk)
    context = {'panorama' : p}
    return render(request, 'panoramas/googleViewer.html',context)

def detailNeighborhood(request,neighborhood_pk):
    neighborhood = NeighboringPanoramas.objects.get(pk=neighborhood_pk)
    context = {'neighborhood' : neighborhood}
    return render(request, 'panoramas/detailNeighborhood.html',context)

# Worker uses this view to get a MapPoint for panorama scraping
def test(request):
    """
    docstring
    """
    while(os.path.isfile('.lock')):
        print('database locked')
        time.sleep(random.uniform(0, 1))
    lock_file = open(".lock","w")
    lock_file.close()

    # Get a MapPoint to pass to worker
    mapPoint = MapPoint.objects.filter(lock=False).first()
    mapPoint.lock=True
    mapPoint.save()

    os.remove(".lock") 
    return HttpResponse(mapPoint)
