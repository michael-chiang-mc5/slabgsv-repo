from django.urls import path
from django.conf.urls import url

from . import views

app_name = 'viewer'
urlpatterns = [
    path('mapPoint/<int:pk>/', views.mapPoint, name='mapPoint'),
    path('objectID/<int:object_id>/', views.objectID, name='objectID'),
    path('panorama/<int:pk>/', views.panorama, name='panorama'),
    path('ocr/<int:pk>/', views.ocr, name='ocr'),
    path('overlaySigns/<int:ocr_pk>/', views.overlaySigns, name='overlaySigns'),
    path('overlaySignsFOV/<int:ocr_pk>/<int:fov>', views.overlaySignsFOV, name='overlaySignsFOV'),
    path('ain/<int:ain>/', views.ain, name='ain'),
    ]
