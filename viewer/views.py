from django.shortcuts import render
from mapPoints.models import *
from panoramas.models import *
from ocr.models import *
from signs.models import *

# Create your views here.
def mapPoint(request,pk):
    m = MapPoint.objects.get(pk=pk)
    print(m)
    context = {'mapPoint' : m}
    return render(request, 'viewer/mapPoint.html',context)

def objectID(request,object_id):
    m = MapPoint.objects.get(object_id=object_id)
    print(m)
    context = {'mapPoint' : m}
    return render(request, 'viewer/mapPoint.html',context)


def panorama(request,pk):
    p = Panorama.objects.get(pk=pk)
    context = {'panorama' : p}
    return render(request, 'viewer/panorama.html',context)


def ocr(request,pk):
    o = Ocr.objects.get(pk=pk)
    context = {'ocr' : o}
    return render(request, 'viewer/ocr.html',context)

def overlaySigns(request,ocr_pk):
    o = Ocr.objects.get(pk=ocr_pk)
    signs = Ocr.objects.get(pk=ocr_pk).sign_set.all()
    context = {'signs': signs, 'ocr' : o}
    return render(request, 'viewer/overlaySigns.html',context)

def overlaySignsFOV(request,ocr_pk,fov):
    o = Ocr.objects.get(pk=ocr_pk)
    signs = Ocr.objects.get(pk=ocr_pk).sign_set.all()
    signs_filtered = []
    for sign in signs:
        x_fraction = sign.x_fraction()
        deg = x_fraction * 360
        if deg > 90-fov/2 and deg<90+fov/2:
            signs_filtered.append(sign)
        if deg > 270-fov/2 and deg<270+fov/2:
            signs_filtered.append(sign)
    context = {'signs': signs_filtered, 'ocr' : o}
    return render(request, 'viewer/overlaySigns.html',context)

def ain(request,ain):
    signs = Sign.objects.filter(ain=ain)
    ocr_pks = signs.values_list('ocr', flat=True).distinct()
    context = {'ocr_pks' : ocr_pks, 'ain': ain}
    return render(request, 'viewer/ain.html',context)